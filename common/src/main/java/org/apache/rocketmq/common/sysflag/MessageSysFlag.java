/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.rocketmq.common.sysflag;

public class MessageSysFlag {
    public final static int COMPRESSED_FLAG = 0x1;  // 0001 压缩
    public final static int MULTI_TAGS_FLAG = 0x1 << 1;  // 0010 多tags
    public final static int TRANSACTION_NOT_TYPE = 0; // 0000 不是事务消息
    public final static int TRANSACTION_PREPARED_TYPE = 0x1 << 2;  // 0100 事务 prepared
    public final static int TRANSACTION_COMMIT_TYPE = 0x2 << 2;  // 1000 事务 commit
    public final static int TRANSACTION_ROLLBACK_TYPE = 0x3 << 2;  // 1100 事务 rollback

    /**
     * 判断是否是事务消息
     * @param flag msg flag
     * @return msg flag
     */
    public static int getTransactionValue(final int flag) {
        return flag & TRANSACTION_ROLLBACK_TYPE;
    }

    /**
     * 根据type，重置消息的flag
     * @param flag msg flag
     * @param type msg type，指出是否是事务消息
     * @return msg flag
     */
    public static int resetTransactionValue(final int flag, final int type) {
        return (flag & (~TRANSACTION_ROLLBACK_TYPE)) | type;
    }

    /**
     * 清除压缩 flag
     * @param flag msg flag
     * @return msg flag
     */
    public static int clearCompressedFlag(final int flag) {
        return flag & (~COMPRESSED_FLAG);
    }
}
